open Multiset.Multiset;;

let ms1 = union (union (singleton "a") (singleton "a")) (singleton "b");;
let ms2 = union (union (singleton "a") (singleton "b")) (singleton "c");;

let ms12 = union ms1 ms2;;
let ms21 = union ms1 ms2;;

let msI12 = intersection ms1 ms2;;
let msI21 = intersection ms2 ms1;;

let t = is_included msI12 ms21;;
let f = is_included ms1 ms2;;

let msD = difference ms21 msI12;;
let msDf = difference ms1 ms2;;

exception Does_not_exist
exception Answer_yes_no
exception Answer_yes
exception Answer_no
exception Evolved

let rec pow a = function
  | 0 -> 1
  | 1 -> a
  | n ->
    let b = pow a (n / 2) in
    b * b * (if n mod 2 = 0 then 1 else a)
;;

let rec apply_times f n x = match n with
| 0 -> x
| _ -> apply_times f (n-1) (f x)
;;

let rec print_string_int_list (l: (string * int) list) = match l with
| [] -> print_string "\n"
| x::q -> print_string ("(" ^ (fst x) ^ ",");
          print_int (snd x);
          print_string (") ; ");
          print_string_int_list q
;;

let rec valuation n l = match l with
| 0 -> []
| _ -> (n mod 2 +1) :: valuation (n/2) (l-1)
;;

let rec valuation_in_matrix n l c = match l with
| 0 -> []
| _ -> valuation (n mod (pow 2 c)) c :: valuation_in_matrix (n / (pow 2 c)) (l-1) c

let reduce i m n =
  let reduce_columns i n =
    let l = List.length (List.hd i) in
    assert (l >= n);
    let rec aux i n = match n with
    | 0 -> i
    | _ -> aux (List.map List.tl i) (n-1)
    in aux i (l-n)
  in
  let reduce_lines i m =
    let k = List.length i in
    assert (k >= m);
    let rec aux i n = match n with
    | 0 -> i
    | _ -> aux (List.tl i) (n-1)
    in aux i (k-m)
  in
  if i = [] || m = 0 then []
  else reduce_columns (reduce_lines i m) n
;;

let get i m n = if m = 0 || n = 0 then 0 else List.nth (List.nth i (m-1)) (n-1);;

type computation_type = Regular | Check_answer | Check_no_evolution;;

type 'a rule = Symp_in of 'a mset | Symp_out of 'a mset | Antiport of ('a mset) * ('a mset) | Division of 'a * 'a * 'a;;

type 'a membrane = {
rules: 'a rule list;
elements: 'a mset;
index_parent: int;
indices_children: int list
  };;

type 'a p_system = 'a membrane array;;



let rec state pi h i k n comp_type =
  match n with
| 0 -> pi.(h).elements
| _ -> let p, y = parent pi h i k n in
       let p, y = ref p, ref y in
       let s' = state pi h (reduce i k (n-1)) k (n-1) Regular in
       let s = ref s' in
       let x = ref empty in
       let ikn = get i k n in
       (* print_string_int_list (to_list !p); *)
       (* print_string_int_list (to_list !y); *)
       (* print_string_int_list (to_list !s); *)
       (* print_string_int_list (to_list !x); *)
       let s_div = apply_division_rule pi h s x ikn in
       if not s_div then
         begin
           if ikn = 2 then raise Does_not_exist;
           apply_communication_rules pi h s x p y
         end;
       contribution_from_children pi h i k n s x;
       (* print_string_int_list (to_list !p); *)
       (* print_string_int_list (to_list !y); *)
       (* print_string_int_list (to_list !s); *)
       (* print_string_int_list (to_list !x); *)
       let s = union !s !x in
       begin
         match comp_type with
       | Check_answer ->
          if k = 0 then
            begin
              match is_included (singleton "yes") !y, is_included (singleton "no") !y with
            | true,true -> raise Answer_yes_no
            | true,false -> raise Answer_yes
            | false,true -> raise Answer_no
            | _ -> ()
            end;
       | Check_no_evolution -> if not ((is_included s s') && (is_included s s')) then
                                 raise Evolved
       | _ -> ()
       end;
       s

and parent pi h i k n = match k with
| 0 -> pi.(0).elements, empty
| _ -> let ik1n = get i (k-1) n in
       let i_red_k1n1 = reduce i (k-1) (n-1) in
       let i_red_k1n = reduce i (k-1) n in
       let ph = pi.(h).index_parent in
       let p = ref (state pi ph i_red_k1n1 (k-1) (n-1) Regular) in
       let y = ref empty in
       let p_div = apply_division_rule pi ph p y ik1n in
       if ik1n = 2 && not p_div then raise Does_not_exist;
       let p', y' = parent pi ph i_red_k1n (k-1) n in
       let p', y' = ref p', ref y' in
       if not p_div then apply_communication_rules pi h p y p' y';
       !p, !y

and contribution_from_children pi h i k n s x =
  let rec aux_membranes m = match m with
  | [] -> ()
  | g::r ->
     for j = 0 to pow 2 (n-1) -1 do
       let i' = (valuation j (n-1))::i in
       try
         let c = ref (state pi g i' (k+1) (n-1) Regular) in
         let y = ref empty in
         let c_div = apply_division_rule pi h s x 1 in
         if not c_div then apply_communication_rules pi g c y s x
       with Does_not_exist -> ()
     done;
     aux_membranes r
  in aux_membranes pi.(h).indices_children

and apply_division_rule pi h s x i =
  let rec aux l = match l with
  | [] -> false
  | Division (a, b, c)::q ->
     if is_included (singleton a) !s then
       begin
         s := difference !s (singleton a);
         x := union !x (singleton (if i = 1 then b else c));
         true
       end
     else
       aux q
  | _::q -> aux q
  in aux pi.(h).rules

and apply_communication_rules pi h s x p y =
  let rec aux l = match l with
  | [] -> ()
  | Symp_out u :: q ->
     while is_included u !s do
       (* print_string "out\n"; *)
       s := difference !s u;
       y := union !y u;
     done;
     aux q
  | Symp_in u :: q ->
     while is_included u !p do
       (* print_string "in\n"; *)
       p := difference !p u;
       x := union !x u
     done;
     aux q
  | Antiport (u, v) :: q ->
     while is_included u !s && is_included v !p do
       (* print_string "out-in\n"; *)
       s := difference !s u;
       y := union !y u;
       p := difference !p v;
       x := union !x v
     done;
     aux q
  | _::q -> aux q
  in aux pi.(h).rules
;;


let rec time_to_answer pi n =
  if n > 10 then failwith "hi";
  try let _ = state pi 1 [] 0 n Check_answer in
      time_to_answer pi (n+1)
  with Answer_yes | Answer_no -> n
;;

let depth_membrane h pi =
    let x = ref h in
    let c = ref 0 in
    while not (!x = 0) do
      x := pi.(!x).index_parent;
      c := !c + 1
    done;
    !c
;;

let depth_system pi =
  let d = ref 0 in
  let m = Array.length pi -1 in
  for h = 0 to m do
    let c = depth_membrane h pi in
    if c > !d then d := c
  done;
  !d
;;

type result = Incorrect | Correct of bool;;

let compute pi =
  let n = time_to_answer pi 0 in
  let m = Array.length pi -1 in
  for h = 1 to m do
    let k = depth_membrane h pi -1 in
    for j = 0 to pow 2 (k * (n+1)) -1 do
      let i = valuation_in_matrix j k (n+1) in
      try let _ = state pi h i k (n+1) Check_no_evolution in ()
      with Does_not_exist -> ();
    done;
  done;
  try let _ = state pi 1 [] 0 n Check_answer in Incorrect
  with Answer_yes -> Correct true | Answer_no -> Correct false
;;

let pi = [|{rules=[]; elements = singleton "a"; index_parent = 0; indices_children = []}|];;
let pi_odd =
  [|
    {rules=[];
     elements = from_list [("c",1000);("d",1000);("y",1000);("z",1000)];
     index_parent = 0;
     indices_children = [1]};
    {rules=[Symp_out (from_list [("a",2)]);
            Symp_out (from_list [("c",1); ("yes",1); ("z",1)]);
            Symp_out (from_list [("d",1); ("no",1); ("z",1)]);
            Antiport (from_list [("x",1)],from_list [("y",1)]);
            Antiport (from_list [("y",1)],from_list [("z",1)]);
            Antiport (from_list [("b",1)],from_list [("c",1)]);
            Antiport (from_list [("a",1);("c",1)],from_list [("d",1)]);
           ];
     elements = from_list [("a",11);("b",1);("no",1);("x",1);("yes",1)];
     index_parent = 0;
     indices_children = []}
  |]
;;
let pi_even =
  [|
    {rules=[];
     elements = from_list [("c",1000);("d",1000);("y",1000);("z",1000)];
     index_parent = 0;
     indices_children = [1]};
    {rules=[Symp_out (from_list [("a",2)]);
            Symp_out (from_list [("c",1); ("yes",1); ("z",1)]);
            Symp_out (from_list [("d",1); ("no",1); ("z",1)]);
            Antiport (from_list [("x",1)],from_list [("y",1)]);
            Antiport (from_list [("y",1)],from_list [("z",1)]);
            Antiport (from_list [("b",1)],from_list [("c",1)]);
            Antiport (from_list [("a",1);("c",1)],from_list [("d",1)]);
           ];
     elements = from_list [("a",18);("b",1);("no",1);("x",1);("yes",1)];
     index_parent = 0;
     indices_children = []}
  |]
;;

let rec binary n l = match l with
| 0 -> ""
| _ -> binary (n/2) (l-1) ^ Int.to_string (n mod 2)
;;

let ceillog2 n = Float.(to_int (ceil (log2 (of_int n))));;

let rec genere_list str n l =
  let rec aux c = match n-c with
  | 0 -> []
  | _ -> (str^(binary c l))::(aux (c+1))
in aux 0
;;

let generate_pi_subsetsum k l =
  let n = List.length l in
  let logn = ceillog2 n in
  let logk = ceillog2 k in
  let max_a' = n + logn + logk + 7 in
  let max_a = n + logn + logk + 1 in
  let max_b = n + logn + logk + 2 in
  let max_c = n + logn + logk + 4 in
  let max_di = n in
  let max_dj = ceillog2 (k+1) in
  let max_dl = n + max_dj + 1 in

  let rules1 =
    List.init (max_a'-1) (fun i -> Antiport (singleton ("a'"^(binary i (ceillog2 max_a'))),
                                             singleton ("a'"^(binary (i+1) (ceillog2 max_a')))))
  in
  let rules2 =
    List.init (max_a-1) (fun i -> Antiport (singleton ("a"^(binary i (ceillog2 max_a))),
                                             double ("a"^(binary (i+1) (ceillog2 max_a)))))
  in
  let rules3 =
    List.init n (fun i -> Antiport (singleton ("b"^(binary i (ceillog2 max_b))),
                                             double ("b"^(binary (i+1) (ceillog2 max_b)))))
  in
  let rules4 =
    List.init (max_b-n-1) (fun i -> Antiport (singleton ("b"^(binary (i+n) (ceillog2 max_b))),
                                             singleton ("b"^(binary (i+n+1) (ceillog2 max_b)))))
  in
  let rules5 =
    List.init n (fun i -> Antiport (singleton ("c"^(binary i (ceillog2 max_c))),
                                             double ("c"^(binary (i+1) (ceillog2 max_c)))))
  in
  let rules6 =
    List.init (max_c-n-1) (fun i -> Antiport (singleton ("c"^(binary (i+n) (ceillog2 max_c))),
                                             singleton ("c"^(binary (i+n+1) (ceillog2 max_c)))))
  in
  let rules7 =
    let acc = ref [] in
    for i = max_di-1 downto 0 do
      for j = max_dj-1 downto 0 do
        for l = n + j - 1 downto 0 do
          acc := (Antiport (singleton ("d"
                                       ^(binary i (ceillog2 max_di))
                                       ^(binary j (ceillog2 max_dj))
                                       ^(binary l (ceillog2 max_dl))),
                            double ("d"
                                    ^(binary i (ceillog2 max_di))
                                    ^(binary j (ceillog2 max_dj))
                                    ^(binary (l+1) (ceillog2 max_dl)))
                 )) :: !acc
        done;
      done;
    done;
    !acc
  in

  let rules12 =
    List.init n (fun i -> Division ("A"^(binary i logn),"B"^(binary i logn),"z"))
  in
  let rules13 =
    List.init n (fun i -> Antiport (singleton ("B"^(binary i logn)),
                                    double ("d"^(binary i (ceillog2 max_di))^(binary 0 (ceillog2 max_dj))^(binary (n+1) (ceillog2 max_dl)))))
  in
  let rules14 =
    let acc = ref [] in
    for i = max_di-1 downto 0 do
      for j = max_dj-1 downto 0 do
        for l = n + j - 1 downto n+1 do
          acc := (Antiport (singleton ("d"
                                       ^(binary i (ceillog2 max_di))
                                       ^(binary j (ceillog2 max_dj))
                                       ^(binary l (ceillog2 max_dl))),
                            double ("d"
                                    ^(binary i (ceillog2 max_di))
                                    ^(binary (j+1) (ceillog2 max_dj))
                                    ^(binary (l+1) (ceillog2 max_dl)))
                 )) :: !acc
        done;
      done;
    done;
    !acc
  in
  let rules15 =
    List.init n (fun i -> Antiport (pair
                         ("d"
                          ^(binary i (ceillog2 max_di))
                          ^(binary (max_dj-1) (ceillog2 max_dj))
                          ^(binary (max_dl-1) (ceillog2 max_dl)))
                         ("v"^(binary i logn)),
                          singleton "p"))
  in


  let env =
    {rules = [] ;
     elements = (from_list
      (List.map (fun x -> x, Int.max_int) (genere_list "a'" max_a' (ceillog2 max_a')
                                           @ (genere_list "a" max_a (ceillog2 max_a))
                                           @ (genere_list "b" max_b (ceillog2 max_b))
                                           @ (genere_list "c" max_c (ceillog2 max_c))
                                           @ (List.concat_map
                                   (fun x -> genere_list x max_dl (ceillog2 max_dl))
                                   (List.concat_map
                         (fun x -> genere_list x max_dj (ceillog2 max_dj))
                         (genere_list "d" max_di (ceillog2 max_di))
                                   )
                                             )
                                           @ ["e";"p"]))
                );
     index_parent = 0;
     indices_children = [1]}
  in
  let skin =
    {rules = [Symp_out (from_list [("a'"^(binary (max_a'-1) (ceillog2 max_a')),1);("n",1);("no",1)]);
              Symp_out (from_list [("m",1);("n",1);("yes",1)]);
              Antiport (singleton ("b"^(binary (max_b-1) (ceillog2 max_b))), singleton "e");
              Antiport (singleton ("a"^(binary (max_a-1) (ceillog2 max_a))), singleton "p")]
             @ rules7
             @ rules6
             @ rules5
             @ rules4
             @ rules3
             @ rules2
             @ rules1;
     elements = (from_list
      (List.map (fun x -> x, 1) (genere_list "a'" 1 (ceillog2 max_a')
                                 @ (genere_list "a" 1 (ceillog2 max_a))
                                 @ (genere_list "b" 1 (ceillog2 max_b))
                                 @ (genere_list "c" 1 (ceillog2 max_c))
                                 @ (List.concat_map
                         (fun x -> genere_list x 1 (ceillog2 max_dl))
                         (List.concat_map
               (fun x -> genere_list x max_dj (ceillog2 max_dj))
               (genere_list "d" max_di (ceillog2 max_di))
                         )
                                   )
                                 @ ["n";"no";"yes"]))
    );
     index_parent = 0;
     indices_children = [2]}
  in
  let mem1 =
    {rules = [Symp_out (from_list [("c"^(binary (max_c-1) (ceillog2 max_c)),1);("e",1);("m",1)]);
              Antiport (singleton "h", singleton ("c"^(binary (max_c-1) (ceillog2 max_c))));
              Symp_out (from_list [("e",1);("q",1)]);
              Symp_out (from_list [("e",1);("p",1)]);
              Antiport (singleton "g", singleton "e");
              Symp_out (from_list [("p",1);("q",1)])]
             @ rules15
             @ rules14
             @ rules13
             @ rules12;
     elements = (from_list ((List.map (fun x -> x, 1) (genere_list "A" n logn))
                            @ [("g",1);("h",1);("m",1);("q",k)]
                            @ (List.map2 (fun x y -> x,y) (genere_list "v" n logn) l))
                );
     index_parent = 1;
     indices_children = []}
  in
  [| env; skin; mem1 |]
;;


(* compute pi_even;; *)
(* compute pi_odd;; *)

let pi_subsetsum_0 = generate_pi_subsetsum 2 [2];;
let pi_subsetsum_1 = generate_pi_subsetsum 10 [1;2;4;5];;

(* to_list (state pi_subsetsum_1 1 [] 0 0 Regular);; *)
(* let l = to_list (state pi_subsetsum_1 0 [] (-1) 0 Regular);; *)
(* let lenl = List.length l;; *)

let env0 = pi_subsetsum_0.(0).elements;;
let skin0 = pi_subsetsum_0.(1).elements;;
let mem10 = pi_subsetsum_0.(2).elements;;
print_string_int_list (to_list env0);;
print_string_int_list (to_list skin0);;
print_string_int_list (to_list mem10);;

print_string "hello";;
(* compute pi_subsetsum_0;; *)

(* for n = 0 to 8 do *)
(*   let t = Sys.time() in *)
(*   let _ = state pi_subsetsum_0 1 [] 0 n Regular in *)
(*   Printf.printf "Execution time: %fs\n" (Sys.time() -. t); *)
(* done; *)

(* for n = 0 to 7 do *)
(*   let t = Sys.time() in *)
(*   let _ = state pi_subsetsum_1 1 [] 0 n Regular in *)
(*   Printf.printf "Execution time: %fs\n" (Sys.time() -. t); *)
(* done; *)
