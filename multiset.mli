module type MULTISET = sig
  type 'a mset
  val empty : 'a mset
  val singleton : 'a -> 'a mset
  val from_list : ('a * int) list -> 'a mset
  val to_list : 'a mset -> ('a * int) list
  val union : 'a mset -> 'a mset -> 'a mset
  val intersection : 'a mset -> 'a mset -> 'a mset
  val cardinal : 'a mset -> int
  val map : ('a -> 'b) -> 'a mset -> 'b mset
  val is_included : 'a mset -> 'a mset -> bool
  val difference : 'a mset -> 'a mset -> 'a mset
  val pair : 'a -> 'a -> 'a mset
  val double : 'a -> 'a mset
end
