module Multiset : MULTISET = struct
  type 'a mset = ('a * int) list

  let empty : 'a mset = []

  let singleton (a: 'a) : 'a mset = [(a,1)]

  let from_list (l: ('a * int) list) : 'a mset = l

  let to_list (ms: 'a mset) : ('a * int) list = ms

  let rec union (ms1: 'a mset) (ms2: 'a mset) : 'a mset = match ms1, ms2 with
  | [], _ -> ms2
  | _, [] -> ms1
  | (a, n) :: ms11 , (b, _) :: _ when a < b -> (a, n) :: union ms11 ms2
  | (a, _) :: _ , (b, m) :: ms22 when a > b -> (b, m) :: union ms1 ms22
  | (a, n) :: ms11, (b, m) :: ms22          -> (a, n + m) :: union ms11 ms22

  let rec intersection (ms1: 'a mset) (ms2: 'a mset) : 'a mset = match ms1, ms2 with
  | [], _ | _, [] -> []
  | (a, _) :: ms11 , (b, _):: _ when a < b -> intersection ms11 ms2
  | (a, _) :: _ , (b, _):: _    when a > b -> intersection ms2 ms1
  | (a, n) :: ms11, (b, m) :: ms22         -> (a, min n m) :: intersection ms11 ms22

  let cardinal (ms: 'a mset) : int = List.fold_left (fun n (_, m) -> n+m) 0 ms

  let map (f: 'a -> 'b) (ms: 'a mset) : 'b mset = List.fold_left (fun acc (a, n) -> union acc [(f a, n)]) empty ms

  let rec is_included (ms1: 'a mset) (ms2: 'a mset) : bool = match ms1, ms2 with
  | [], _ -> true
  | _, [] -> false
  | (a, n) :: ms11 , (b, _) :: _   when a < b -> false
  | (a, _) :: _ , (b, m) :: ms22   when a > b -> is_included ms1 ms22
  | (a, n) :: ms11, (b, m) :: ms22 when n > m -> false
  | (a, n) :: ms11, (b, m) :: ms22            -> is_included ms11 ms22

  let rec difference (ms1: 'a mset) (ms2: 'a mset) : 'a mset =
    assert (is_included ms2 ms1);
    match ms1, ms2 with
    | _, [] -> ms1
    | [], _ -> failwith "impossible"
    | (a, n) :: ms11 , (b, _) :: _   when a < b -> (a, n) :: difference ms11 ms2
    | (a, _) :: _ , (b, m) :: ms22   when a > b -> failwith "impossible"
    | (a, n) :: ms11, (b, m) :: ms22 when n < m -> failwith "impossible"
    | (a, n) :: ms11, (b, m) :: ms22 when n = m -> difference ms11 ms22
    | (a, n) :: ms11, (b, m) :: ms22            -> (a, n-m) :: difference ms11 ms22

  let pair (a: 'a) (b:'a) : 'a mset = union (singleton a) (singleton b)

  let double (a: 'a) : 'a mset = pair a a
end
