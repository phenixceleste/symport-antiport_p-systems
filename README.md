# Symport/antiport P-system simulator

This project allows to simulate the computation of a symport/antiport P-system.
A lot of things have to be cleaned, and it will be interesting to rewrite and structure the code.

This implementation is purely experimental, it is inconceivable to use it for real.

Implementing memoization is an idea to make the computation more effective but it is not the principal objective of this project.
